This is a docker container orchestration to:

- Watch for movies & series releases using radarr & sonarr
- Automatically download new releases using deluge (radarr/sonarr adds these to deluge automatically)
- And these files are then automatically added to jellyfin for easy consumption.
- The deluge downloads & jackett torrent lookups run through a vpn.

# Configuration

**Before running**: run `cp .env.sample .env` and edit the values in the new `.env` file

The configurations listed here are config settings related to making these containers interoperate.  
You'll probably will need to so some extra configuring regarding min/max files sizes to be downloaded in sonarr & radarr and to set it to automatically remove the completed downloads from the download client. This you will have to configure in the tools themselves.  
But for that you should look at those tools's documentation.

## VPN

You'll need to configure the vpn container as described [here](https://github.com/dperson/openvpn-client)
(The `./config/vpn` is mounted under `/vpn`)

### PIA example

For PIA the configuration is as follows:

- [ ] Download PIA's [config zip](https://www.privateinternetaccess.com/openvpn/openvpn.zip).
- [ ] Copy the .crt, .pem and the .ovpn file of your choice to the `./config/vpn` folder.
      **Important:** the .ovpn file cannot have spaces in it's name.
- [ ] In the .ovpn file, replace `auth-user-pass` with `auth-user-pass pass.txt`
- [ ] Then create the `pass.txt` (`./config/vpn/pass.txt`) file with the contents:
  ```
  pia_username_here
  pia_password_here
  ```

## Deluge config

Deluge URL http://localhost:8112

- [ ] Enable the labels plugin in deluge (For radarr, sonarr support)

## Radarr, Sonarr config

Radarr URL http://localhost:7878
Sonarr URL http://localhost:8989

- [ ] Add deluge as a download client in the settings
      (use `deluge` as host)
- [ ] Use `/movies` as the "movies" folder in Radarr
- [ ] Use `/series` as the "series" folder in Sonarr

## Jellyfin config

Jellyfin URL http://localhost:8096
TODO: update this readme part
- [ ] Use `/movies` for the "Movies" library
- [ ] Use `/series` for the "TV Shows" library

# TODO

Other containers to possibly add:
- https://docs.linuxserver.io/images/docker-lidarr